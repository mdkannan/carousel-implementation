# Carousel implementation

A basic Carousel implemented by using html5,CSS3 and java script;
 
## Useful Features

* This can be supported on all the following devices:
  ## Desk top
  ## Tabplet
  ## Mobile

 ##Run:
 
  # Need to open the index.html on the broswer then Carousel will be your view.
  #If not able to see it
        check with the image url for rendering on the view(optional);
 
 ##Usage:
  # You can navigate the right arrow and left arrow to see the carousel pictures.
  # You can view it on the mobile browser
  # you can use this feature on Tablet.