"use-strict"

var leftValue = []; 
var curPos = 0;
var totalSlides = 0;
//Json structure for showing the contents in the view.
var list =   [{"img":"slide1.jpg", "text1": "236 bestemmingen", 
                      "text2": "Zoek op budget, periode, regio en de locative van uw Facebook vrienden wereldwijd. Een groot netwerk betekent veel herinneringen. Een groot netwerk betekent veel herinneringen.", 
                      "text3":"Begin met zoeken"
              },
              {"img":"slide2.jpg", "text1": "237 bestemmingen", 
                      "text2": "Zoek op budget, periode, regio en de locative van. Een groot netwerk betekent veel herinneringen. Een groot netwerk betekent veel herinneringen.", 
                      "text3":"Begin met zoeken"
              },
              {"img":"slide3.jpg", "text1": "238 bestemmingen", 
                      "text2": "Zoek op budget, periode, regio en de locative van uw Facebook vrienden wereldwijd. Een groot netwerk betekent veel herinneringen.", 
                      "text3":"Begin met zoeken"
              },
              {"img":"slide4.jpg", "text1": "239 bestemmingen", 
                      "text2": "Zoek op budget, periode, regio en de locative van uw Facebook vrienden wereldwijd. Een groot netwerk betekent veel herinneringen.", 
                      "text3":"Begin met zoeken"
              },
              {"img":"slide5.jpg", "text1": "240 bestemmingen", 
                      "text2": "Zoek op budget, periode. Een groot netwerk betekent veel herinneringen.", 
                      "text3":"Begin met zoeken"
              },
              {"img":"slide6.jpg", "text1": "241 bestemmingen", 
                      "text2": "Zoek op budget. Een groot netwerk betekent veel herinneringen.", 
                      "text3":"Begin met zoeken"
              },
               {"img":"slide7.jpg", "text1": "242 bestemmingen", 
                      "text2": "Zoek op budget. Een groot netwerk betekent veel herinneringen.", 
                      "text3":"Begin met zoeken"
              }];
var tmplImage = '<div id="slide{{0}}" class="slide {{1}}"></div>';
var tmplText = '<section class="text"><div class="headtxt">{{1}}</div><div class="desctxt">{{2}}</div><div class="linktxt">{{3}}</div></section>';
var tmplBullet = '<div id="list{{0}}" data-pos="{{1}}" class="list {{2}}"></div>';
            
for(var lst in list) { 
  var index = parseInt(lst) + 1;
  var className = "";
  if(index==1) { className = "in"; }
  var strText1 = list[lst].text1;
  var strText2 = list[lst].text2;
  var strText3 = list[lst].text3;

  var strImageSection = tmplImage.replace('{{0}}', index).replace('{{1}}', className);
  document.getElementsByClassName("container")[0].innerHTML += strImageSection;

  var strTextSection = tmplText.replace('{{1}}', strText1).replace('{{2}}', strText2).replace('{{3}}', strText3);
  document.getElementsByClassName("block2")[0].innerHTML += strTextSection;

  className = "";
  if(index==1) { className = "active"; }
  var strBulletSection = tmplBullet.replace('{{0}}', index).replace('{{1}}', index).replace('{{2}}', className);
  document.getElementsByClassName("block3list")[0].innerHTML += strBulletSection;
  leftValue.push((-lst*100) + "%");
  

  var lst = document.getElementsByClassName('list');
  for(var i=0;i<lst.length;i++){
    lst[i].addEventListener('click', function(e) {
      curPos = parseInt(this.getAttribute("data-pos")) - 1;
      listSlide(curPos);
    });
  }

  
}

totalSlides = list.length;
document.getElementsByClassName("container")[0].style.width = (totalSlides*100) + "%";

document.getElementsByClassName("block3list")[0].style.width = (totalSlides*13) + "px";

var iLeft = 0, iLeftinc = 100/list.length;
for(var lst in list) {
  document.getElementsByClassName("slide")[lst].style.left = iLeft + "%";
  document.getElementsByClassName("slide")[lst].style.width =  "calc(100% / " + list.length + ")";
  document.getElementsByClassName("slide")[lst].style.backgroundImage = "url('./images/" + list[lst].img + "')";
  iLeft += iLeftinc;
}

document.getElementsByClassName("text")[0].style.opacity = "1";

function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}

function removeAllClass(pos){
  /*Text Properties*/
  var txtList = document.getElementsByClassName("text");
  for(var i=0;i<txtList.length;i++){
    txtList[i].style.opacity = "0";
  }
  var txt = document.querySelectorAll(".text:nth-child(" + (parseInt(pos)+1) + ")");
  txt[0].style.opacity = "1";

  /*Bullet Properties*/
  var list1 = document.getElementsByClassName('list');
  for(var i=0;i<list1.length;i++) {
    removeClass(list1[i], 'active');
  }
  var listId = document.getElementById('list' + (parseInt(pos)+1));
  addClass(listId, 'active');
}

function leftSlide() {
  if(curPos == 0 ) { return; }
  curPos--;
  removeAllClass(curPos);
  document.getElementsByClassName('container')[0].style.left = leftValue[curPos];
}

function rightSlide() {
  if(curPos == (totalSlides-1) ) { return; }
  curPos++;
  removeAllClass(curPos);
  document.getElementsByClassName('container')[0].style.left = leftValue[curPos];
}

function listSlide(i){
  if(screen.width<500) { return; }
  removeAllClass(i);
  document.getElementsByClassName('container')[0].style.left = leftValue[i];
}